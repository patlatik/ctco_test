import EventsDispatcher from 'events-dispatcher';
import AppConstants from '../AppConstants.js';

class ConfigProvider extends EventsDispatcher {

  async load() {
    return fetch(AppConstants.configName)
      .then((response) => response.json())
      .then((configData) => {
        this._configData = configData;
        this.trigger(AppConstants.events.CONFIG_LOADED, [configData]);
      })
      .catch((error) => {
        this.trigger(AppConstants.events.CONFIG_FAILED, [error]);
      });
  }

  get configData() {
    return this._configData;
  }

}

export default ConfigProvider;
