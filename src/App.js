import React from 'react';
import ConfigProvider from './services/ConfigProvider.js';
import BackgroundView from './views/BackgroundView.js';
import LoadingMessageView from './views/LoadingMessageView.js';
import WidgetView from './views/WidgetView.js';
import AppConstants from './AppConstants.js';

class App extends React.Component {

  constructor(props) {
    super(props);
    
    this.state = {
      error: false,
      loading: true
    }

    this.configProvider = new ConfigProvider();
    this.configProvider.on(AppConstants.events.CONFIG_LOADED, this.onConfigLoaded, this);
    this.configProvider.on(AppConstants.events.CONFIG_FAILED, this.onConfigFailed, this);
    this.configProvider.load();
  }

  onConfigLoaded() {
    this.setState({
      loading: false
    });
  }

  onConfigFailed() {
    this.setState({
      error: true,
      loading: false
    });
  }

  render() {
    if (this.state.error) {
      return(
        <div>
          <BackgroundView/>
          <LoadingMessageView>Config loading failed!</LoadingMessageView>
        </div>
      );
    }

    if (this.state.loading) {
      return(
        <div>
          <BackgroundView/>
          <LoadingMessageView>Loading Config...</LoadingMessageView>
        </div>
      );
    }

    return(
      <div>
        <BackgroundView/>
        <WidgetView widgetConfig={this.configProvider.configData.widgets[0]}/>
      </div>
      
    );
  }

  componentWillUnmount() {
    this.configProvider.off(AppConstants.events.CONFIG_LOADED, this.onConfigLoaded);
    this.configProvider.off(AppConstants.events.CONFIG_FAILED, this.onConfigFailed);
  }
}

export default App;
