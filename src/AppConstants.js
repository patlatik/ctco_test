export const AppConstants = {
  configName: 'data.json',
  itemTypes: {
    currency: "currency"
  },
  currencyMapping: {
    USD: "$",
    EUR: "€",
    GBP: "£"
  },
  events: {
    CONFIG_LOADED: "CONFIG_LOADED",
    CONFIG_FAILED: "CONFIG_FAILED"
  }
}

export default AppConstants;