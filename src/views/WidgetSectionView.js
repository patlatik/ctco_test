import React from 'react';
import WidgetItemView from './WidgetItemView.js';

class WidgetSectionView extends React.Component {

  constructor(props) {
    super(props);
    this.sectionConfig = props.sectionConfig;
  }

  prepareItems(columns, itemsConfig) {
    const result = [];
    let rowItems = [];
    for (let i = 0; i < itemsConfig.length; i++) {
      rowItems.push(React.createElement(WidgetItemView, {
        key: i,
        itemConfig: itemsConfig[i]
      }));
      if (rowItems.length === columns || i === itemsConfig.length - 1) {
        result.push(
          <div className="itemsRow" key={result.length}>{rowItems}</div>
        );
        rowItems = [];
      }

    }
    return result;  
  }

  render() {
    const items = this.prepareItems(this.sectionConfig.columns, this.sectionConfig.items);
    return(
      <div className="section">
          <div className="header">{this.sectionConfig.header}</div>
          <div className="data">{items}</div>
      </div>
    );
  }

}

export default WidgetSectionView;
