import React from 'react';
import AppConstants from '../AppConstants.js';
import classNames from 'classnames';

class WidgetItemView extends React.Component {

  constructor(props) {
    super(props);
    this.itemConfig = props.itemConfig;
    this.isCurrencyType = this.itemConfig.type === AppConstants.itemTypes.currency;
    this.currencySymbol = this.formatCurrency(this.itemConfig.symbol);
    this.handleChange = this.handleChange.bind(this);
    this.handleBlur = this.handleBlur.bind(this);
    this.state = {
      value: this.formatValue(this.itemConfig.value)
    };
  }

  formatValue(value) {
    if (this.isCurrencyType) {
      let numValue = Number(value);
      if (value === "" || isNaN(numValue)) {
        value = "0";
        numValue = 0;
      }
      if (this.itemConfig.precision) {
        //multiplier used to fix problem with rounding
        const precisionMultiplier = Math.pow(10, this.itemConfig.precision);
        return (Math.floor(numValue * precisionMultiplier) / precisionMultiplier).toFixed(this.itemConfig.precision);
      }
    }
    return value;
  }

  formatCurrency(currency) {
    return AppConstants.currencyMapping[currency] ? AppConstants.currencyMapping[currency] : currency;
  }

  handleChange(event) {
    if (this.isCurrencyType) {
      const checkNumberFormatRex = /^[0-9.\b]+$/;
      if (event.target.value !== '' && !checkNumberFormatRex.test(event.target.value)) {
        return;
      }        
    }
    this.setState({
      value: event.target.value
    });
  }

  handleBlur() {
    this.setState({
      value: this.formatValue(this.state.value)
    });
  }

  render() {
    const inputElements = [];
    var inputClasses = classNames({
      'input': true,
      'inputWithCurrency': this.isCurrencyType,
      'inputWithShortCurrency': this.isCurrencyType && this.currencySymbol.length === 1
    });

    if (this.isCurrencyType) {
      inputElements.push(
        <span className="currency" key="currency">{this.currencySymbol}</span>
      );
    }
    inputElements.push(
      <input type="text" key="input" className={inputClasses} value={this.state.value} onChange={this.handleChange} onBlur={this.handleBlur}></input>
    );

    const inputRequiredElements = [];
    if (this.itemConfig.required) {
      inputRequiredElements.push(
        <span className="inputRequired" key="star">⚹</span>
      );
    }

    return(
      <div className="itemCell">
        <div className="label">
          {inputRequiredElements}
          {this.itemConfig.label}
        </div>
        <div className="inputFrame">{inputElements}</div>
      </div>
    );
  }

}

export default WidgetItemView;
