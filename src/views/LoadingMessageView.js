import React from 'react';

class LoadingMessageView extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      message: String(props.children)
    }
  }

  componentWillReceiveProps(props) {
    this.setState({
      message: String(props.children)
    });
  }

  render() {
    return(
        <div className="introContaiter">
          <div className="logo"></div>
          <div className="message">{this.state.message}</div>
        </div>
    );
  }

}

export default LoadingMessageView;
