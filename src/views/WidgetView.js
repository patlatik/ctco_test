import React from 'react';
import WidgetSectionView from "./WidgetSectionView.js";

class WidgetView extends React.Component {

  constructor(props) {
    super(props);
    this.widgetConfig = props.widgetConfig;
  }

  prepareSections(sectionsConfig) {
    const result = [];
    for (let i = 0; i < sectionsConfig.length; i++) {
      result.push(React.createElement(WidgetSectionView, {
        key: i,
        sectionConfig: sectionsConfig[i]
      }));
    }
    return result;  
  }

  render() {
      const sections = this.prepareSections(this.widgetConfig.items);
      return(
        <div className="widget">
          <div className="header">{this.widgetConfig.name}</div>
          <div className="sections">{sections}</div>
        </div>
      );
  }
}

export default WidgetView;
